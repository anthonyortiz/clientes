import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Cliente {
  nombres: string;
  apellidos: string;
  tipo_doc: string;
  num_doc: string;
  fecha_nac: string;
  observacion: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConexionService {

  private itemsCollection: AngularFirestoreCollection<Cliente>;
  private clienteDoc: AngularFirestoreDocument<Cliente>;
  cliente: Observable<Cliente[]>;
  constructor(private afs: AngularFirestore) {
    this.itemsCollection = afs.collection<Cliente>('cliente');
    this.cliente = this.itemsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Cliente;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  retornaClientes() {
    return this.cliente;
  }
  addCliente(cliente: Cliente) {
    this.itemsCollection.add(cliente);
  }

  eliminar(id) {
    this.clienteDoc = this.afs.doc<Cliente>("cliente/" + id);
    this.clienteDoc.delete();
  }

  editar(cliente) {
    this.clienteDoc = this.afs.doc<Cliente>("cliente/" + cliente.id);
    this.clienteDoc.update(cliente);
  }

}
