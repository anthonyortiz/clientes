import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConexionService } from '../../services/conexion.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() edit: any = {
    nombres: '',
    apellidos: '',
    tipo_doc: '',
    num_doc: '',
    fecha_nac: '',
    observacion: ''
  };

  cliente: any = {
    nombres: '',
    apellidos: '',
    tipo_doc: '',
    num_doc: '',
    fecha_nac: '',
    observacion: ''
  };
  constructor(private con: ConexionService) { }

  ngOnInit(): void {
  }

  agregar(): void {
    this.con.addCliente(this.cliente);
    this.limpiarCampos();
  }

  editarCliente(): void {
    this.con.editar(this.edit);
    this.limpiarCampos();
  }

  limpiarCampos(): void {
    this.cliente = {
      nombres: '',
      apellidos: '',
      tipo_doc: '',
      num_doc: '',
      fecha_nac: '',
      observacion: ''
    };

    this.edit = {
      nombres: '',
      apellidos: '',
      tipo_doc: '',
      num_doc: '',
      fecha_nac: '',
      observacion: ''
    };

  }
}
