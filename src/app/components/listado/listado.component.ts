import { Component, OnInit } from '@angular/core';
import { ConexionService } from '../../services/conexion.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  clientes: any;
  idCliente = '';

  constructor(private con: ConexionService) {
    this.con.retornaClientes().subscribe(clientes => {
      this.clientes = clientes;
      console.log(this.clientes);
    });
  }

  ngOnInit(): void {
  }


  eliminar(id): void {
    this.con.eliminar(id);
  }


  enviar(id): void {
    this.idCliente = id;
  }
}
