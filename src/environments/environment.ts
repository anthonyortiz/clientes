// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDS6BS-DPOxT3y2cj8ib48d3tq21kgrFiY",
    authDomain: "clientesfire-6dffb.firebaseapp.com",
    databaseURL: "https://clientesfire-6dffb.firebaseio.com",
    projectId: "clientesfire-6dffb",
    storageBucket: "clientesfire-6dffb.appspot.com",
    messagingSenderId: "302892022943",
    appId: "1:302892022943:web:612c74060b02e1846736aa",
    measurementId: "G-5ZNZFBZ80M"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
